/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung72;

import StdLib.StdDraw;

/**
 *
 * @author nicogiessmann
 */
public class Uebung72 {

    private static final double Y_SCALE = 1.2;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StdDraw.setXscale(0, 1);
        StdDraw.setYscale(0, 1);
        final double MIN_SIZE = 0.05;
        final double MAX_SIZE = 0.15;
        
        for(int i=0; i<4; i++){
            double size = (MAX_SIZE-MIN_SIZE) * Math.random() + MIN_SIZE;
            double r_x = size + (Math.random() * (1-2*size));
            double r_y = Y_SCALE*size + (Math.random() * (1-2*Y_SCALE*size));
            printDiamonds(r_x, r_y, size);
        }
    }
    
    public static void printDiamonds(double x, double y, double size){
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.filledPolygon(new double[]{x,x+size,x,x-size}, new double[]{y+Y_SCALE*size,y,y-Y_SCALE*size,y});   
    }
    
}
