/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung63;

import StdLib.*;

/**
 *
 * @author nicogiessmann
 */
public class Uebung63 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        if(Math.abs(k)>26)k=k%26;
        String eingabe = StdIn.readAll();
        for(int i=0; i<eingabe.length(); i++){
            int cp = eingabe.codePointAt(i);
            if(cp>=65 & cp<=90){
                cp+=k;
                if(cp>90)cp-=26;
                if(cp<65)cp+=26;
            }
            else if(cp>=97 & cp<=122){
                cp+=k;
                if(cp>122)cp-=26;
                if(cp<97)cp+=26;
            }
            StdOut.print(Character.toString((char)cp));
        }
        StdOut.println();
    }
   
}
