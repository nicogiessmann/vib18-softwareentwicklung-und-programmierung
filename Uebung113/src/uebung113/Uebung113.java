/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung113;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Uebung113 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StdOut.println("Sonderposten:");
        Sonderposten bellantines = new Sonderposten("SC120-C", 8.0, 2, "BALLANTINES"); 
        Sonderposten jack = new Sonderposten("SC123-F", 10.0, 6, "JACK DANIELS"); 
        Sonderposten glen = new Sonderposten("SC347-A", 55.0, 15, "GLEN MORANGIE"); 
        bellantines.anzeigen();
        jack.anzeigen();
        glen.anzeigen();
              
    }
    
}
