/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung113;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Sonderposten extends Artikel {
    
    private double rabatt;
    
    public Sonderposten(String artikelNr, double einkaufspreis, int lagerzeit, String bezeichnung) {
        super(artikelNr, einkaufspreis, lagerzeit, bezeichnung);
        if(lagerzeit > 12){
            rabatt = 0.30;
        }
        else{
            rabatt = 0.10;
        }
    }

    @Override
    public void anzeigen() {
        super.anzeigen();
        StdOut.println("(VK <alt>: " + super.berechneVerkaufspreis() + " Euro; Rabatt: " + (int)(rabatt*100) + "%)");
    }

    @Override
    public int berechneVerkaufspreis() {
        int vk = super.berechneVerkaufspreis();
        return (int)((double)vk - rabatt*vk);
    }
    
    
    
    
    
}
