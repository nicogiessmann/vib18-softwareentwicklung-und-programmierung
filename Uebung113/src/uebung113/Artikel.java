/*
 * Uebung 10 Aufgabe 3
 */
package uebung113;

import StdLib.StdOut;

/**
 * Diese Klasse beschreibt einen Artikel.
 *
 * @author nicogiessmann, lorenzgerlich
 */
public class Artikel {

    /*
    * Instanzvariablen werden definiert. Diese sind für jeden Objekt der Klasse "Artikel" unabhängig.
     */
    protected double einkaufspreis;
    protected int lagerzeit;
    protected String bezeichnung, artikelNr;
    /*
    * Konstanten werden definiert. Diese sind unabänderlich und für jedes Objekt der Klasse Artikel identisch.
    * Optimierung: Da die Konstanten im Klassekopf initialisiert werden, können sie zusätzlich statisch sein.
    * Die MWST ist gesetzlich auf einen Regelsatz festgelegt.
     */
    final static double HANDELSSPANNE = 0.6; // 60 Prozent des Einkaufspreises
    final static double MWST = 0.19; // 19 Prozent

    public Artikel(String artikelNr, double einkaufspreis, int lagerzeit, String bezeichnung) {
        this.artikelNr = artikelNr;
        this.einkaufspreis = einkaufspreis;
        this.lagerzeit = lagerzeit;
        this.bezeichnung = bezeichnung;
    }

    public void anzeigen() {
        StdOut.println(artikelNr + " \"" + bezeichnung + "\" EK: " + einkaufspreis + " VK: " + berechneVerkaufspreis() + " Euro Lagerzeit: " + lagerzeit + " Monate");
    }

    public int berechneVerkaufspreis() {
        return (int) Math.floor((einkaufspreis + HANDELSSPANNE * einkaufspreis) * (1.0 + MWST));
    }

}
