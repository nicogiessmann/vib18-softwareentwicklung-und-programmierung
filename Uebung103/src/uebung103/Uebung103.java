/*
 * uebung 10 Aufgabe 3
 */
package uebung103;

import Artikel.Artikel;
import Artikel.ArtikelExt;
import StdLib.StdOut;

/**
 * Diese Klasse testet die Funktionalitäten der Klassen Artikel und ArtikelExt.
 *
 * @author nicogiessmann
 */
public class Uebung103 {

    public static void main(String[] args) {
        //Normale Artikel
        Artikel bellantines = new Artikel("SC120-C", 8.0, 2, "BELLANTINES");
        Artikel jackdaniels = new Artikel("SC123-F", 10.0, 6, "JACK DANIELS");
        Artikel glenmrangie = new Artikel("SC347-A", 55.0, 15, "GLEN MORANGIE");
        bellantines.anzeigen();
        jackdaniels.anzeigen();
        glenmrangie.anzeigen();

        StdOut.println("---");
        //Ext Artikel
        ArtikelExt bellantines_ext = new ArtikelExt(bellantines);
        ArtikelExt jackdaniels_ext = new ArtikelExt(jackdaniels);
        ArtikelExt glenmrangie_ext = new ArtikelExt(glenmrangie);
        bellantines_ext.anzeigen();
        jackdaniels_ext.anzeigen();
        glenmrangie_ext.anzeigen();

        ArtikelExt.aendereHandelsspanne(0.8);

        StdOut.println("---");

        bellantines_ext.anzeigen();
        jackdaniels_ext.anzeigen();
        glenmrangie_ext.anzeigen();
    }

}
