/*
 * Uebung 10 Aufgabe 3
 */
package Artikel;

/**
 * Diese Klasse erweitert Artikel um eine veränderliche Handelsspanne.
 *
 * @author nicogiessmann
 */
public class ArtikelExt extends Artikel {

    private static double handelsspanne = Artikel.HANDELSSPANNE;

    public ArtikelExt(String artikelNr, double einkaufspreis, int lagerzeit, String bezeichnung) {
        super(artikelNr, einkaufspreis, lagerzeit, bezeichnung);
    }

    public ArtikelExt(Artikel art) {
        super(art.artikelNr, art.einkaufspreis, art.lagerzeit, art.bezeichnung);
    }

    @Override
    public int berechneVerkaufspreis() {
        return (int) Math.floor((einkaufspreis + ArtikelExt.handelsspanne * einkaufspreis) * (1.0 + MWST));
    }

    public static void aendereHandelsspanne(double handelsspanne) {
        ArtikelExt.handelsspanne = handelsspanne;
    }

}
