package uebung64;

import StdLib.*;
import java.awt.Color;

public class BouncingBall {

    public static void main(String[] args) {
        double rx = 0;
        double ry = .8;
        double vx = .2;
        double vy = .1;
        double radius = .2;
        final double GRAVITY = -.005;
        final double FRICTION = -.0005;

        StdDraw.setXscale(-1.0, +1.0);
        StdDraw.setYscale(-1.0, +1.0);

        while (true) {

            boolean collison_x = false, collison_y = false;

            if ((ry - radius + vy + GRAVITY) <= -1.0) {
                collison_y = true;
                ry = -1.0 + radius;
                vy = -vy;
                StdAudio.play("punch.wav");
                if (Math.abs(vy) < 0.01) {
                    break;
                }
            } else if ((ry + radius + vy + GRAVITY) >= 1.0) {
                collison_y = true;
                ry = 1.0 - radius;
                vy = -vy;
                StdAudio.play("punch.wav");
            }

            if ((rx - radius + vx) <= -1.0) {
                collison_x = true;
                rx = -1.0 + radius;
                vx = -vx;
                StdAudio.play("slap.wav");
            } else if ((rx + radius + vx) >= 1.0) {
                collison_x = true;
                rx = 1.0 - radius;
                vx = -vx;
                StdAudio.play("slap.wav");
            }

            //horizontal friction, set limit for h. friction
            if (Math.abs(vx) <= .0001) {
                vx = 0;
            } else {
                if (vx > 0) {
                    //air friction
                    vx += FRICTION;
                    //extra friction when bounce
                    if (collison_x) {
                        vx += FRICTION * 10;
                    }
                } else if (vx < 0) {
                    //air friction
                    vx -= FRICTION;
                    //extra friction when bounce
                    if (collison_x) {
                        vx -= FRICTION * 10;
                    }
                }
            }

            //vertical friction, no need for limit because of gravity
            if (vy > 0) {
                //air friction
                vy += FRICTION;
                //extra friction when bounce
                if (collison_y) {
                    vy = Math.abs(vy) + FRICTION * 10;
                }
            } else if (vy < 0) {
                //air friction
                vy -= FRICTION;
                //extra friction when bounce
                if (collison_y) {
                    vy = -Math.abs(vy) + FRICTION * 10;
                }
            }

            //gravity
            if (!collison_y) {
                vy += GRAVITY;
            }

            //move ball
            if (!collison_x) {
                rx = rx + vx;
            }
            if (!collison_y) {
                ry = ry + vy;
            }

            StdDraw.setPenColor(new Color(132, 207, 152));
            StdDraw.filledSquare(0.0, 0.0, 1.0);
            StdDraw.picture(rx, ry, "avocado.png", radius*2, radius*2);
            StdDraw.show(20);

        }
    }
}
