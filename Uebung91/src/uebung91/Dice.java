/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung91;

/**
 *
 * @author nicogiessmann
 */
public class Dice {

    private static final int[] values = {1, 2, 3, 4, 5, 6};
    private int valueUp;

    public Dice() {
        valueUp = getRandomValue();
    }

    private static int getRandomValue() {
        return values[(int) Math.round(Math.random() * (values.length - 1))];
    }

    public void roll() {
        valueUp = getRandomValue();
    }

    public int getValueUp() {
        return valueUp;
    }

}
