/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung91;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Uebung91 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 0; i < 30; i++) {
            Dice wuerfel = new Dice();
            sum += wuerfel.getValueUp();
        }
        StdOut.println("Gesamtzahl > " + sum);
    }

}
