/*
 * Uebung 10 Aufgabe 2
 */
package uebung102_main;

import StdLib.StdOut;
import java.awt.Point;
import java.util.Arrays;
import uebung102_rectangles.RectangleFromBottomRight;
import uebung102_rectangles.RectangleFromCenter;
import uebung102_rectangles.RectangleFromTwoPoints;

/**
 * Diese Klasse beinhaltet die main-Methode und testet die Rectangle-Datentypen.
 *
 * @author nicogiessmann
 */
public class Uebung102 {

    public static void main(String[] args) {
        RectangleFromTwoPoints rect1 = new RectangleFromTwoPoints(new Point(-3, -3), new Point(3, 3));
        StdOut.println(rect1.toString());
        RectangleFromBottomRight rect2 = new RectangleFromBottomRight(new Point(3, -3), 6, 6);
        StdOut.println(rect2.toString());
        RectangleFromCenter rect3 = new RectangleFromCenter(new Point(0, 0), 6, 6);
        //Spezialfunktionen: Fläche und Eckpunkte berechnen
        StdOut.println(rect3.toString());
        StdOut.println(rect1.getSurfaceArea() + "," + rect2.getSurfaceArea() + "," + rect3.getSurfaceArea());
        StdOut.println(Arrays.toString(rect1.getEdges()) + "\n" + Arrays.toString(rect2.getEdges()) + "\n" + Arrays.toString(rect3.getEdges()));
    }

}
