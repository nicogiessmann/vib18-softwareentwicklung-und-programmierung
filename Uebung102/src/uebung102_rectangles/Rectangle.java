/*
 * Uebung 10 Aufgabe 2
 */
package uebung102_rectangles;

import java.awt.Point;

/**
 * Diese Klasse definiert mein eigenes Rechteck und ist Superkasse der
 * speziellen Rechtecke.
 *
 * @author nicogiessmann
 */
public class Rectangle {

    private int x_left, y_top, width, height;

    protected void initRectangle(int x_left, int y_top, int width, int height) {
        this.x_left = x_left;
        this.y_top = y_top;
        this.width = width;
        this.height = height;
    }

    //In case, super constructor with parameters cannot be called in first place
    public Rectangle() {
    }

    ;
    
    @Override
    public String toString() {
        return "Rectangle: x_left=" + x_left + "; y_top=" + y_top + "; width=" + width + "; height=" + height;
    }

    public int getSurfaceArea() {
        return width * height;
    }

    public Point[] getEdges() {
        return new Point[]{new Point(x_left, y_top), new Point(x_left + width, y_top), new Point(x_left + width, y_top - height), new Point(x_left, y_top - height)};
    }
}
