/*
 * Uebung 10 Aufgabe 2
 */
package uebung102_rectangles;

import java.awt.Point;

/**
 * Diese Klasse definiert ein Rechteck durch den unteren, rechten Punkt, Breite
 * und Höhe.
 *
 * @author nicogiessmann
 */
public class RectangleFromBottomRight extends Rectangle {

    public RectangleFromBottomRight(Point bottomRightPoint, int width, int height) {
        initRectangle(bottomRightPoint.x - width, bottomRightPoint.y + height, width, height);
    }
}
