/*
 * Uebung 10 Aufgabe 2
 */
package uebung102_rectangles;

import java.awt.Point;

/**
 * Diese Klasse definiert ein Rechteck aus zwei diagonalen Punkten.
 *
 * @author nicogiessmann
 */
public class RectangleFromTwoPoints extends Rectangle {

    public RectangleFromTwoPoints(Point ecke1, Point ecke2) {
        //Local substitues for super class variabl
        int x_left, y_top, width, height;

        //Determine x_left and width
        if (ecke1.x <= ecke2.x) {
            x_left = ecke1.x;
            width = ecke2.x - x_left;
        } else {
            x_left = ecke2.x;
            width = ecke1.x - x_left;
        }

        //Determine y_top and height
        if (ecke1.y >= ecke2.y) {
            y_top = ecke1.y;
            height = y_top - ecke2.y;
        } else {
            y_top = ecke2.y;
            height = y_top - ecke1.y;
        }

        initRectangle(x_left, y_top, width, height);
    }
}
