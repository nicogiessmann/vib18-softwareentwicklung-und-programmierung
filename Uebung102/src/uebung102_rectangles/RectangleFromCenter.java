/*
 * Uebung 10 Aufgabe 2
 */
package uebung102_rectangles;

import java.awt.Point;

/**
 * Diese Klasse definiert ein Rechteck durch den Mittelpunkt, Breite und Höhe.
 *
 * @author nicogiessmann
 */
public class RectangleFromCenter extends Rectangle {

    public RectangleFromCenter(Point centerPoint, int width, int height) {
        initRectangle(centerPoint.x - width / 2, centerPoint.y + height / 2, width, height);
    }
}
