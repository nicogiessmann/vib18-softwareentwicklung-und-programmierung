/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung73;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Uebung73 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int M = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        double p = Double.parseDouble(args[2]);
        boolean[][] minefield = new boolean[M][N];
        for (int x = 0; x < M; x++) {
            for (int y = 0; y < N; y++) {
                double r = Math.random();
                if (r <= p) {
                    minefield[x][y] = true;
                    StdOut.print(" * ");
                } else {
                    minefield[x][y] = false;
                    StdOut.print(" . ");
                }
            }
            StdOut.println();
        }
        StdOut.println();
        for (int x = 0; x < M; x++) {
            for (int y = 0; y < N; y++) {
                if (minefield[x][y]) {
                    StdOut.print(" * ");
                } else {
                    StdOut.print(" " + getMinesAround(minefield, x, y, M, N) + " ");
                }
            }
            StdOut.println();
        }

    }

    private static int getMinesAround(boolean[][] minefield, int x, int y, int M, int N) {
        int minesaround = (isMine(minefield, x - 1, y + 1, M, N) ? 1 : 0) + 
                (isMine(minefield, x - 1, y, M, N) ? 1 : 0) + 
                (isMine(minefield, x - 1, y - 1, M, N) ? 1 : 0) + 
                (isMine(minefield, x, y + 1, M, N) ? 1 : 0) + 
                (isMine(minefield, x, y, M, N) ? 1 : 0) + 
                (isMine(minefield, x, y - 1, M, N) ? 1 : 0) + 
                (isMine(minefield, x + 1, y + 1, M, N) ? 1 : 0) + 
                (isMine(minefield, x + 1, y, M, N) ? 1 : 0) + 
                (isMine(minefield, x + 1, y - 1, M, N) ? 1 : 0);
        return minesaround;
    }

    private static boolean isMine(boolean[][] minefield, int t_x, int t_y, int M, int N) {
        if (t_x < 0 | t_x > M - 1) {
            return false;
        }
        if (t_y < 0 | t_y > N - 1) {
            return false;
        }
        return minefield[t_x][t_y];
    }

}
