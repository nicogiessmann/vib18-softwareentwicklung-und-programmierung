/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung93;

/**
 *
 * @author nicogiessmann
 */
public class Date {

    private int day_int, month_int;
    private Year year;
    /*
    * Array 'daysPerMonth' definiert die Anzahl der Tage in jedem Monat.
     */
    private static final int[] daysPerMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    /*
    * Konstruktor ist mit Exceptions versehen. Grund: Falsche Datumseingaben
    * können nicht sinnvoll interpretiert werden.
     */
    public Date(int day, int month, int year) throws DateInputException {
        this.year = new Year(year);
        if (month > 0 & month <= 12) {
            month_int = month;
            int maxDays = daysPerMonth[month_int - 1];
            if (month_int == 2 & this.year.isLeapYear()) {
                maxDays = 29;
            }
            if (day > 0 & day <= maxDays) {
                day_int = day;
            } else {
                throw new DateInputException("Day must be between 1 and " + maxDays + "! Input was " + day + ".");
            }
        } else {
            throw new DateInputException("Month must be between 1 and 12! Input was " + month + ".");
        }
    }

    @Override
    public String toString() {
        return day_int + "." + month_int + "." + year.toString();
    }

    /*
    * Liefert die Tage, die in diesem Jahr schon seit dem Datum vergangen sind.
     */
    public int getNumberOfDay() {
        int sum = day_int;
        for (int i = 0; i < month_int - 1; i++) {
            if (i == 1 & year.isLeapYear()) {
                sum += 29;
            } else {
                sum += daysPerMonth[i];
            }
        }
        return sum;
    }

    /*
    * Prüft, ob das vermeindlich größere Datum auch wirklich größer ist.
     */
    private boolean isSmallerGreaterThanGreater(Date greaterDate, Date smallerDate) {
        if (smallerDate.getYear().getYear_Int() > greaterDate.getYear().getYear_Int()) {
            return true;
        } else if (smallerDate.getYear().getYear_Int() == greaterDate.getYear().getYear_Int()) {
            if (smallerDate.getMonth_Int() > greaterDate.getMonth_Int()) {
                return true;
            } else if (smallerDate.getMonth_Int() == greaterDate.getMonth_Int()) {
                if (smallerDate.getDay_Int() > greaterDate.getDay_Int()) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
    * Für den Fall, dass das größere und kleinere Datum vertauscht werden, 
    * ist der Betrag der Differenz gleichbleibend, aber mit einem Minus versehen.
     */
    public int getDaysSince(Date date) {
        Date greaterDate = this;
        Date smallerDate = date;
        boolean changeOrder = isSmallerGreaterThanGreater(this, date);
        if (changeOrder) {
            greaterDate = date;
            smallerDate = this;
        }

        int yearDates = 0;
        for (int y = smallerDate.getYear().getYear_Int(); y < greaterDate.getYear().getYear_Int(); y++) {
            yearDates += Year.getTotalDaysThisYear(y);
        }
        int sum = greaterDate.getNumberOfDay() - smallerDate.getNumberOfDay() + yearDates;
        if (changeOrder) {
            return -sum;
        }
        return sum;
    }

    /*
    * Zugriff auf Year als int nur über Spezialklasse möglich.
     */
    public Year getYear() {
        return year;
    }

    public int getMonth_Int() {
        return month_int;
    }

    public int getDay_Int() {
        return day_int;
    }

}
