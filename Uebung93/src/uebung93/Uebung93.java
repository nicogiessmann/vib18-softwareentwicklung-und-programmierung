/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung93;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Uebung93 {

    public static void main(String[] args) {
        
        /*
        * Exception wird aufgefangen, wenn ungültiges Datum eigegeben.
        */
        Date datum1 = null;
        try {
            datum1 = new Date(26, 5, 2019);
        } catch (DateInputException ex) {
            System.err.println(ex.getMessage());
        }
        Date datum2 = null;
        try {
            datum2 = new Date(31, 5, 2020);
        } catch (DateInputException ex) {
            System.err.println(ex.getMessage());
        }

        /*
        * Es wird nur weitergearbeitet, wenn beide Datumsangaben korrekt sind.
        */
        if (datum1 != null & datum2 != null) {
            StdOut.println(datum2.getDaysSince(datum1));
        }
    }

}
