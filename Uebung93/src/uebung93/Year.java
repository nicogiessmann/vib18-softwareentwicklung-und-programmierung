/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung93;

/**
 *
 * @author nicogiessmann
 */
public class Year {

    private int year_int;

    public Year(int year) {
        year_int = year;
    }

    @Override
    public String toString() {
        return year_int + "";
    }

    public int getYear_Int() {
        return year_int;
    }

    public boolean isLeapYear() {
        return isLeapYear(year_int);
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            }
            return true;
        }
        return false;
    }

    public int getTotalDaysThisYear() {
        return getTotalDaysThisYear(year_int);
    }

    public static int getTotalDaysThisYear(int year) {
        if (isLeapYear(year)) {
            return 366;
        }
        return 365;
    }

}
