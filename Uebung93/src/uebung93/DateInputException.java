/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung93;

/**
 *
 * @author nicogiessmann
 * 
 * Diese Exception wird ausgelöst, wenn das Datum ungültig ist. Also der Monat
 * nicht zwischen 1 und 12 liegt oder der Tag in diesem Monat nicht existiert.
 */
public class DateInputException extends Exception {

    public DateInputException(String error) {
        super("Falsches Datumsformat bei der Eingabe!");
        System.err.println(error);
    }
}
