/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung61;

import StdLib.*;

/**
 *
 * @author nicogiessmann
 */

public class PlayThatTune {

    public static void main(String[] args) {
        double lautstaerke = Double.parseDouble(args[0]);
        double abspielgeschwindigkeit = Double.parseDouble(args[1]);
        
        while (!StdIn.isEmpty()) {
            int pitch = StdIn.readInt();
            double seconds = StdIn.readDouble();
            double hz = 440.0 * Math.pow(2, pitch / 12.0);

            int SAMPLE_RATE = 44100;
            int N = (int) (seconds / abspielgeschwindigkeit * SAMPLE_RATE);
            double[] a = new double[N + 1];
            for (int i = 0; i <= N; i++) {
                a[i] = lautstaerke * Math.sin(2 * Math.PI * i * hz / SAMPLE_RATE);
            }
            StdAudio.play(a);
        }
    }
}
