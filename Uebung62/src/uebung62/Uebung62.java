/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung62;

import StdLib.*;

/**
 *
 * @author nicogiessmann
 */
public class Uebung62 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*Einlesen des Textes von StdIn*/
        String text = StdIn.readAll();
        /*Mehrfache Leerzeichen werden gegen eines vertauscht*/
        text = text.replaceAll("\\s{2,}", " ");
        /*Woerter werden anhand eines Leerzeichens aufgesplittet*/
        String[] woerter = text.split("\\s");
        /*Ausgabe Anzahl Woerter*/
        StdOut.println("Woerter > " + woerter.length);
        /*Verbleibende Leerzeichen werden geloescht*/
        text = text.replaceAll("\\s", "");
        /*Ausgabe Anzahl Zeichen*/
        StdOut.println("Zeichen > " + text.length());
    }
    
}
