/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung92;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Uebung92 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int range_min = 0;
        int range_max = 100;
        Point3D[] points = new Point3D[3];
        for (int i = 0; i < points.length; i++) {
            int r_x = range_min + (int) Math.round(Math.random() * (range_max - range_min));
            int r_y = range_min + (int) Math.round(Math.random() * (range_max - range_min));
            int r_z = range_min + (int) Math.round(Math.random() * (range_max - range_min));
            points[i] = new Point3D(r_x, r_y, r_z, i + "");
            points[i].printPosition();
        }

        StdOut.println("---");

        for (int i = 0; i < points.length; i++) {
            for (int s = i + 1; s < points.length; s++) {
                StdOut.println("Euklidischer Abstand [" + i + ";" + s + "] = " + points[i].getEuklidischerAbstand(points[s]));
                StdOut.println("Quad. Euklidischer Abstand [" + i + ";" + s + "] = " + points[i].getQuadEuklidischerAbstand(points[s]));
                StdOut.println("Manhatten Metrik [" + i + ";" + s + "] = " + points[i].getManhattenMetrik(points[s]));
            }
        }
    }
}
