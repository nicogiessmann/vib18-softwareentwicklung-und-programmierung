/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung92;

import StdLib.StdOut;

/**
 *
 * @author nicogiessmann
 */
public class Point3D {

    public double x, y, z;
    private String name;

    public Point3D(double x, double y, double z, String name) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.name = name;
    }

    public double getEuklidischerAbstand(Point3D p) {
        return Math.sqrt(getQuadEuklidischerAbstand(p));
    }

    public double getQuadEuklidischerAbstand(Point3D p) {
        return Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2) + Math.pow(z - p.z, 2);
    }

    public double getManhattenMetrik(Point3D p) {
        return Math.abs(x - p.x) + Math.abs(y - p.y) + Math.abs(z - p.z);
    }

    public void printPosition() {
        StdOut.println("Position von Punkt " + name + ": [" + x + "|" + y + "|" + z + "]");
    }
}
