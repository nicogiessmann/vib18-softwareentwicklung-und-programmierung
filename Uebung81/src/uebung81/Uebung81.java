/*
 * Übungsblatt 8 Aufgabe 1
 */
package uebung81;

import StdLib.StdOut;
import java.util.Arrays;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uebung81 {

    public static void main(String[] args) {
        double ymin_test = 10;
        double ymax_test = 100;
        double[] a_test = {1,3,5.5,7,8.2,10};
        double[] erg_test = linearScaling(ymin_test, ymax_test, a_test);
        StdOut.println(Arrays.toString(erg_test));
    }
    
    private static double[] linearScaling(double ymin, double ymax, double[] a){
        if(ymin>=ymax)return new double[]{}; 
        double scale = ymax/ymin;
        double[] res = new double[a.length];
        for(int i=0; i<a.length; i++){
            res[i] = scale * a[i];
        }
        return res;
    }
    
}
