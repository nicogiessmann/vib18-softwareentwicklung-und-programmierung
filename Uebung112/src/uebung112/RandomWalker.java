/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung112;

/**
 *
 * @author nicogiessmann
 */
public class RandomWalker {
    private static final int ORIGIN_X = 0, ORIGIN_Y = 0;
    private int x = ORIGIN_X, y = ORIGIN_Y;
    
    public void step(){
        int r = (int) Math.round(Math.random()*4);
        switch(r){
            case 0: //top
                y++;
                break;
            case 1: //left
                x--;
                break;
            case 2: //bottom
                y--;
                break;
            case 3: //right
                x++;
        }
    }
    
    public double distance(){
        int diff_x = ORIGIN_X - x;
        int diff_y = ORIGIN_Y - y;
        return Math.sqrt(Math.pow(diff_x, 2)+Math.pow(diff_y, 2));
    }
}
