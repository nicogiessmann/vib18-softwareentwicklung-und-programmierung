/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung112;

/**
 *
 * @author nicogiessmann
 */
public class Uebung112 {

    private static final int N = 500000;
    private static final int I = 10000;
    
    public static void main(String[] args) {
        
        
        RandomWalker[] walkers = new RandomWalker[I];
        for(int i=0; i<I; i++){
            walkers[i] = new RandomWalker();
        }
        
        for(int n=0; n<N; n++){
            for(int i=0; i<I; i++){
                walkers[0].step();
            }
        }
        
        int summe=0;
        for(int i=0; i<I; i++){
            summe+=walkers[i].distance();
        }
        
        System.out.println((double)summe/(double)I);
    }
    
}
