/*
 * Uebungsblatt 8 Aufgabe 2
 */
package uebung82;

import StdLib.StdDraw;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uebung82 {

    public static void main(String[] args) {
        
        int x=0;
        int y=0;
        
        StdDraw.setXscale(-100, 100);
        StdDraw.setYscale(-100, 100);
        StdDraw.filledSquare(x, y, 0.5);
        
        for(int kaestchen=1; kaestchen<153; kaestchen++){
            for(int move=0; move<kaestchen; move++){
                switch(kaestchen%4){
                    case 3: //up
                        y--;
                        break;
                    case 2: //right
                        x++;
                        break;
                    case 1: //down
                        y++;
                        break;
                    case 0: //left
                        x--;
                        break;
                }
                StdDraw.filledSquare(x, y, 0.5);
            }
        }
        
        
    }
    
}
