/*
 * Uebung 10 Aufgabe 1
 */
package uebung101;

import java.util.Date;

/**
 *
 * @author nicogiessmann (Kommentare)
 */
public class Appointment {

    /*
    * Die Klasse Appointment soll einen Verabredung abspeichern. Dabei ist das Datum
    * und der Kunde vertraulich zu behandeln, weshalb die Instanzvariablen als private
    * deklariert wurden.
     */
    private Date date;
    private String customer;

    public Appointment(Date date) {
        // e.g. check that date is in some legal range
        this.date = date;
    }

    /*
    * Der Getter zur privaten Variable date ermöglicht es trotz privater Kapselung
    * den Wert von date zu erhalten. Dadurch geht die ursprüngliche Intention, den Termin
    * vertraulich zu verhandeln verloren. Andererseits muss ein Getter explizit vom Entwickler
    * hinzugefügt werden. Daraus schließe ich, dass der Wert von date keine sehr vertrauliche
    * Information ist und somit abgerufen werden kann.
     */
    public Date getDate() {
        return date;
    }
}
