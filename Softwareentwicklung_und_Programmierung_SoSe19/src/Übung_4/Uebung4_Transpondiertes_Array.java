/*
*  Anhang 1
 */
package Übung_4;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uebung4_Transpondiertes_Array {

    public static void main(String[] args) {
        //Zweidimensionales Array mit Zufallswerten initialisieren
        int[][] ursprung = new int[5][5];
        for (int x = 0; x < ursprung.length; x++) {
            for (int y = 0; y < ursprung[0].length; y++) {
                ursprung[x][y] = (int) (Math.random() * 100);
            }
        }
        System.out.println("Das ist das Ursprungsarray:");
        arrayAusgeben(ursprung);
        System.out.println();
        //Ursprungsarray transpondieren
        int[][] transpondiert = new int[5][5];
        for (int x = 0; x < transpondiert.length; x++) {
            for (int y = 0; y < transpondiert[0].length; y++) {
                transpondiert[x][y] = ursprung[y][x];
            }
        }
        System.out.println("Das ist das transpondierte Array:");
        arrayAusgeben(transpondiert);
    }

    /*
    * Diese Funktion gibt ein zweidimensionales Array auf sysout aus.
     */
    private static void arrayAusgeben(int[][] array) {
        for (int y = 0; y < array[0].length; y++) {
            for (int x = 0; x < array.length; x++) {
                System.out.print(array[x][y] + "|");
            }
            System.out.println();
        }
    }
}
