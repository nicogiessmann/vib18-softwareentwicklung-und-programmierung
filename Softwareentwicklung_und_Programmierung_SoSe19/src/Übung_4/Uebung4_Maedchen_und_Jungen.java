/*
 * Anhang 3
 */
package Übung_4;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uebung4_Maedchen_und_Jungen {

    public static void main(String[] args) {

        int N = 1000000;
        int summe_geburten = 0;
        int genau_zwei_geburten = 0;
        int genau_drei_geburten = 0;
        int genau_vier_geburten = 0;
        int fuenf_oder_mehr_geburten = 0;
        
        for (int i = 0; i < N; i++) {
            boolean junge_bekommen = false;
            boolean maedchen_bekommen = false;
            int geburten = 0;
            while (!(junge_bekommen & maedchen_bekommen)) {
                int geschlecht = (int) Math.round(Math.random());
                if (geschlecht < 0.5) {
                    junge_bekommen = true;
                } else {
                    maedchen_bekommen = true;
                }
                geburten++;
            }
            summe_geburten+=geburten;
            switch(geburten){
                case 2:
                    genau_zwei_geburten++;
                    break;
                case 3:
                    genau_drei_geburten++;
                    break;
                case 4:
                    genau_vier_geburten++;
                    break;
                default:
                    fuenf_oder_mehr_geburten++;
                    break;
            }
        }
        
        //Durchschnittliche Geburten berechnen
        double durchschnitt_geburten = (double) summe_geburten/(double) N;
        System.out.println("Durchschnittliche Geburten: " + durchschnitt_geburten);
        
        //Mittlere Häufigkeit besonderer Werte
        double haeufigkeit_zwei_geburten = (double) genau_zwei_geburten/(double) N;
        System.out.println("Häufigkeit genau zwei Geburten: " + haeufigkeit_zwei_geburten);
        
        double haeufigkeit_drei_geburten = (double) genau_drei_geburten/(double) N;
        System.out.println("Häufigkeit genau drei Geburten: " + haeufigkeit_drei_geburten);
        
        double haeufigkeit_vier_geburten = (double) genau_vier_geburten/(double) N;
        System.out.println("Häufigkeit genau vier Geburten: " + haeufigkeit_vier_geburten);
        
        double haeufigkeit_mehr_geburten = (double) fuenf_oder_mehr_geburten/(double) N;
        System.out.println("Häufigkeit fünf oder mehr Geburten: " + haeufigkeit_mehr_geburten);
    }
}