/*
 * Anhang 2
 */
package Übung_4;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uebung4_Wuerfel_Simulation {

    public static void main(String[] args) {

        //Vorgabe: Wahrscheinlichkeitsverteilung Würfel
        double[] dist = new double[13];
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j <= 6; j++) {
                dist[i + j] += 1.0;
            }
        }
        for (int k = 1; k <= 12; k++) {
            dist[k] /= 36.0;
        }

        //Simulation
        int N = Integer.parseInt(args[0]);
        int[] anzahl_augen = new int[13];
        for (int i = 0; i < N; i++) {
            int wuerfel_eins = (int) ((Math.random() * 6 + 1));
            int wuerfel_zwei = (int) ((Math.random() * 6 + 1));
            int summe = wuerfel_eins + wuerfel_zwei;
            anzahl_augen[summe]++;
        }
        //Ausgabe: Wie oft wurde jede Summe geworfen
        //System.out.println(Arrays.toString(anzahl_augen));
        //Wandlung in Wahrscheinlichkeit
        double[] rel = new double[13];
        for (int i = 0; i < rel.length; i++) {
            rel[i] = (double) anzahl_augen[i] / (double) N;
            System.out.printf("%.3f", rel[i]);
            System.out.print("|");
            System.out.printf("%.3f", dist[i]);
            System.out.println();
        }
    }

}
