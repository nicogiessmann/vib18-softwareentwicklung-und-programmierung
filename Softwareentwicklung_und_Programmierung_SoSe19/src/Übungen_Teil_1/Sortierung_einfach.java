//Anhang 2
package Übungen_Teil_1;

import java.util.Arrays;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Sortierung_einfach {

    public static void main(String[] args) {
        int[] vorher = new int[4];
        vorher[0] = Integer.parseInt(args[0]);
        vorher[1] = Integer.parseInt(args[1]);
        vorher[2] = Integer.parseInt(args[2]);
        vorher[3] = Integer.parseInt(args[3]);
        System.out.println("Vorher:     " + Arrays.toString(vorher));

        if (vorher[0] > vorher[1]) {
            int pos1 = vorher[1];
            vorher[1] = vorher[0];
            vorher[0] = pos1;
        }
        if (vorher[1] > vorher[2]) {
            int pos2 = vorher[2];
            vorher[2] = vorher[1];
            vorher[1] = pos2;
        }
        if (vorher[2] > vorher[3]) {
            int pos3 = vorher[3];
            vorher[3] = vorher[2];
            vorher[2] = pos3;
        }
        
        if (vorher[0] > vorher[1]) {
            int pos1 = vorher[1];
            vorher[1] = vorher[0];
            vorher[0] = pos1;
        }
        if (vorher[1] > vorher[2]) {
            int pos2 = vorher[2];
            vorher[2] = vorher[1];
            vorher[1] = pos2;
        }

        if (vorher[0] > vorher[1]) {
            int pos1 = vorher[1];
            vorher[1] = vorher[0];
            vorher[0] = pos1;
        }
        
        System.out.println("Vorher:     " + Arrays.toString(vorher));
    }
}
