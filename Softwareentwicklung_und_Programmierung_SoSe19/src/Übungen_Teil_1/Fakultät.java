/*
 * Anhang 2
 */
package Übungen_Teil_1;

/**
 *
 * @author nicogiessmann, lorenzgerlich
 */
public class Fakultät {
    public static void main(String[] args){
        int zahl = Integer.parseInt(args[0]);
        int fakultaet = 1;
        
        for(int i=1; i<=zahl; i++){
            fakultaet = fakultaet * i;
        }
        
        System.out.println(fakultaet);
        
    }
}
