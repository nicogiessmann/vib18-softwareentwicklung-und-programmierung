/*
 * Anhang 4
 */
package Übungen_Teil_1;

/**
 *
 * @author nicogiessmann, lorenzgerlich
 */
public class Zaehlen {
    public static void main(String[] args){
        
        for(int i=1000; i<2000; i+=5){
            System.out.print(i+" ");
            System.out.print(i+1+" ");
            System.out.print(i+2+" ");
            System.out.print(i+3+" ");
            System.out.println(i+4);
        }
    }
}
