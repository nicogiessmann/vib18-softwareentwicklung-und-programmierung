/*
 * Anhang 3
 */
package Übungen_Teil_1;

/**
 *
 * @author nicogiessmann, lorenzgerlich
 */
public class MaxFakultät {
    public static void main(String[] args){
        int zahl=0;
        int fakultaet = 1;
        
        while(fakultaet < 1000){
            fakultaet = 1;      
            for(int i=1; i<=zahl; i++){
                fakultaet = fakultaet * i;
            }
            zahl++;   
        }
        
        System.out.println(zahl-1);
    }
}
