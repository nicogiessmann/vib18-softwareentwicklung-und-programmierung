//Anhang 3
package Übungen_Teil_1;

/**
 *
 * @author nicogiessmann, lorenzgerlich
 */
public class Schleifen_Unterschied {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean programmStart=false;
        
        //While-Schleife
        System.out.println("While Schleife Output:");
        while(programmStart){
            System.out.println("Programm läuft...");
        }
        System.out.println("Programm beendet.");
        
        //Do-while Schleife
        System.out.println("Do-while Schleife Output:");
        do{
            System.out.println("Programm läuft...");
        } while(programmStart);
        System.out.println("Programm beendet.");
    }
    
}
