/*
 * Anhang 3
 */
package Übung_5;

import StdLib.*;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Aufgabe3Heart {

    public static void main(String[] args) {
        // Kreise rot zeichnen
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.filledCircle(0.7, 0.7, 0.2);
        StdDraw.filledCircle(0.3, 0.7, 0.2);
        // Spitze zeichnen
        double x[] = {0.117, 0.5, 0.883};
        double y[] = {0.62, 0.1, 0.62};
        StdDraw.filledPolygon(x, y);
        // Leerraum füllen
        StdDraw.filledCircle(0.5, 0.6, 0.1);

    }

}
