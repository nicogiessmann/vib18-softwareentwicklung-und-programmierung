/*
 * Anhang 2
 */
package Übung_5;

import StdLib.StdIn;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Werte_einlesen {

    public static void main(String[] args) {
        int maximum = 0, minimum = 0;
        int[] allewerte = new int[10000];

        int i = 0;
        while (StdIn.hasNextLine()) {
            int wert = StdIn.readInt();
            allewerte[i] = wert;
            if (i == 0) {
                maximum = wert;
                minimum = wert;
            } else {
                if (wert > maximum) {
                    maximum = wert;
                } else if (wert < minimum) {
                    minimum = wert;
                }
            }
            i++;
        }

        System.out.println("Maximum: " + maximum);
        System.out.println("Minimun: " + minimum);
        int summe = 0;
        for (i = 0; i < 10000; i++) {
            summe += allewerte[i];
        }
        int mittelwert = summe / 10000;
        System.out.println("Mittelwert: " + mittelwert);
        double varianz = 0;
        for (i = 0; i < 10000; i++) {
            varianz += Math.pow(allewerte[i] - mittelwert, 2);
        }
        varianz /= 10000;
        varianz = Math.sqrt(varianz);
        System.out.println("Varianz: " + varianz);
    }
}
