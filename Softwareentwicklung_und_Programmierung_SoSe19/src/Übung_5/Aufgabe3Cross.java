/*
 * Anhang 4
 */
package Übung_5;

import StdLib.*;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Aufgabe3Cross {

    public static void main(String[] args) {
        // Dreiecksfuß zeichnen
        double x[] = {0.25, 0.5, 0.75};
        double y[] = {0.05, 0.35, 0.05};
        StdDraw.filledPolygon(x, y);
        // Fuß abrunden mit weißen Kreisen
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.filledCircle(0.21, 0.35, 0.28);
        StdDraw.filledCircle(0.79, 0.35, 0.28);
        // Schwarze Kreise zeichnen
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.filledCircle(0.5, 0.8, 0.2);
        StdDraw.filledCircle(0.32, 0.45, 0.2);
        StdDraw.filledCircle(0.68, 0.45, 0.2);
        StdDraw.filledCircle(0.5, 0.6, 0.1);
        // Fuß mit Kreisen verbinden
        StdDraw.filledRectangle(0.5, 0.25, 0.01, 0.15);
    }
}
