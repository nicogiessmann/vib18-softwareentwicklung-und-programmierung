/*
 * Anhang 5
 */
package Übung_5;

import StdLib.StdDraw;
import java.awt.Color;
import java.awt.Font;
import java.time.LocalDateTime;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uhr {

    public static void main(String[] args) {
        //Konstanten für Zeigerlängen & Dicken
        final double STUNDENZEIGER = 0.2;
        final double MINUTENZEIGER = 0.30;
        final double SEKUNDENZEIGER = 0.4;
        final double STUNDENZEIGER_D = 0.01;
        final double MINUTENZEIGER_D = 0.004;
        final double SEKUNDENZEIGER_D = 0.002;
        final double TWOPI = 6.283;

        while (true) {
            //Bild refreshen
            StdDraw.clear(new Color(255, 255, 255));

            //Uhrkörper zeichnen
            StdDraw.setPenRadius(0.01);
            StdDraw.circle(0.5, 0.5, 0.5);
            StdDraw.setFont(new Font("Times New Roman", Font.BOLD, 28));
            StdDraw.text(0.15, 0.49, "9");
            StdDraw.text(0.5, 0.15, "6");
            StdDraw.text(0.5, 0.83, "12");
            StdDraw.text(0.85, 0.49, "3");

            //Stundenlinien zeichnen
            for (int i = 0; i < 12; i++) {
                double edgeX = Math.sin((double) i / 12.0 * TWOPI) * 0.49 + 0.5;
                double edgeY = Math.cos((double) i / 12.0 * TWOPI) * 0.49 + 0.5;
                double endX = Math.sin((double) i / 12.0 * TWOPI) * 0.4 + 0.5;
                double endY = Math.cos((double) i / 12.0 * TWOPI) * 0.4 + 0.5;
                StdDraw.setPenRadius(STUNDENZEIGER_D);
                StdDraw.line(edgeX, edgeY, endX, endY);
            }

            //Minutenlinien zeichnen
            for (int i = 0; i < 60; i++) {
                if (!(i == 0 | i % 5 == 0)) {
                    double edgeX = Math.sin((double) i / 60.0 * TWOPI) * 0.5 + 0.5;
                    double edgeY = Math.cos((double) i / 60.0 * TWOPI) * 0.5 + 0.5;
                    double endX = Math.sin((double) i / 60.0 * TWOPI) * 0.45 + 0.5;
                    double endY = Math.cos((double) i / 60.0 * TWOPI) * 0.45 + 0.5;
                    StdDraw.setPenRadius(MINUTENZEIGER_D);
                    StdDraw.line(edgeX, edgeY, endX, endY);
                }
            }

            //Designkreis für Mitte zeichnen
            StdDraw.filledCircle(0.5, 0.5, 0.01);

            //Stundenzeiger zeichnen
            double stunde = LocalDateTime.now().getHour();
            if (stunde > 12.0) {
                stunde -= 12.0;
            }
            double stundeX = Math.sin(stunde / 12.0 * TWOPI) * STUNDENZEIGER + 0.5;
            double stundeY = Math.cos(stunde / 12.0 * TWOPI) * STUNDENZEIGER + 0.5;
            StdDraw.setPenRadius(STUNDENZEIGER_D);
            StdDraw.line(0.5, 0.5, stundeX, stundeY);

            //Minutenzeiger zeichnen
            double minute = LocalDateTime.now().getMinute();
            double minuteX = Math.sin(minute / 60.0 * TWOPI) * MINUTENZEIGER + 0.5;
            double minuteY = Math.cos(minute / 60.0 * TWOPI) * MINUTENZEIGER + 0.5;
            StdDraw.setPenRadius(MINUTENZEIGER_D);
            StdDraw.line(0.5, 0.5, minuteX, minuteY);

            //Sekundenzeiger zeichnen
            double sekunde = LocalDateTime.now().getSecond();
            double sekundeX = Math.sin(sekunde / 60.0 * TWOPI) * SEKUNDENZEIGER + 0.5;
            double sekundeY = Math.cos(sekunde / 60.0 * TWOPI) * SEKUNDENZEIGER + 0.5;
            StdDraw.setPenRadius(SEKUNDENZEIGER_D);
            StdDraw.line(0.5, 0.5, sekundeX, sekundeY);

            //Warten
            StdDraw.show(1000);
        }
    }
}
