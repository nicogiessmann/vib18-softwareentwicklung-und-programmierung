/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Übung_5;

import StdLib.StdDraw;
import java.awt.Color;

/**
 *
 * @author nicogiessmann
 */
public class Kreuz {
    public static void main(String[] args){
        StdDraw.filledPolygon(new double[]{0,0.5,1}, new double[]{0,0.5,0});
        StdDraw.setPenColor(Color.WHITE);
        StdDraw.filledCircle(1, 0.5, 0.5);
        StdDraw.filledCircle(0, 0.5, 0.5);
        StdDraw.setPenColor(Color.BLACK);
        StdDraw.filledCircle(0.5, 0.7, 0.2);
        StdDraw.filledCircle(0.3, 0.4, 0.2);
        StdDraw.filledCircle(0.8, 0.4, 0.2);
    }
}
