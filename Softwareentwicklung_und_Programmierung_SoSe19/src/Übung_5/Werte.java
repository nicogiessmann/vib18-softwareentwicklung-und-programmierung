/*
 * Anhang 1
 */
package Übung_5;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Werte {

    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        for (int i = 0; i < N; i++) {
            int zufallszahl = (int) (Math.random() * 100);
            System.out.println(zufallszahl);
        }
    }
}
