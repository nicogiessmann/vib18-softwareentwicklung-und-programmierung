/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uebung71;

import StdLib.StdOut;
import java.util.Arrays;

/**
 *
 * @author nicogiessmann
 */
public class Uebung71 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StdOut.println("Uneven: " + uneven(true, true, true));
        StdOut.println("Any: " + any(new boolean[]{true, true, false, true, true}));
        StdOut.println("All: " + all(new boolean[]{false, true, true, false, false}));
    }

    private static boolean uneven(boolean wert1, boolean wert2, boolean wert3) {
        return (wert1 ^ wert2) ^ wert3;
    }

    private static boolean any(boolean[] werte) {
        if (werte.length > 1) {
            return werte[0] | any(Arrays.copyOfRange(werte, 1, werte.length));
        }
        return werte[0];
    }

    private static boolean all(boolean[] werte) {
        if (werte.length > 1) {
            return werte[0] & all(Arrays.copyOfRange(werte, 1, werte.length));
        }
        return werte[0];
    }

}
