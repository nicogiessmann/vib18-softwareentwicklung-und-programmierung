/*
 * Uebungsblatt 8 Aufgabe 3
 */
package uebung83;

import StdLib.StdOut;

/**
 * @author nicogiessmann, lorenzgerlich
 */
public class Uebung83 {

    /**
     * @param args Unvollständige Kundenummer
     */
    public static void main(String[] args) {
        String kn = args[0]+getChecksum(args[0]);
        StdOut.println(kn);
    }
    
    private static int quersumme(String zahlenstring){
        int summe = 0;
        for(int i=0; i<zahlenstring.length(); i++){
            int zahlAnI = Integer.parseInt(zahlenstring.charAt(i)+"");
            summe+=zahlAnI;
        }
        return summe;
    }
    
    private static int f(int d){
        return quersumme(2*d+"");
    }
    
    private static int getModSum(String kn){
        int summe=0;
        for(int i=0; i<kn.length(); i++){
            int zahlAnI = Integer.parseInt(kn.charAt(i)+"");
            if(i%2==0){
                summe+=zahlAnI;
            }
            else{
                summe+=f(zahlAnI);
            }
        }
        return summe;
    }
    
    private static int getChecksum(String zahl){
        int sum = getModSum(zahl);
        for(int i=0; i<=9; i++){
            if((sum+i)%10==0)return i;
        }
        return -1;
    }
    
    private static boolean checkCustomer(String kn){
        return getModSum(kn)%10==0;
    }
    
}
